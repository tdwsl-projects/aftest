// simple affine texture mapper
// click on center of canvas to start spinning

let cvs = document.getElementById("canvas");
let ctx = canvas.getContext("2d");

const tex = [
    "#00f","#00f","#00f","#00f","#f0f","#00f","#00f","#00f","#00f",
    "#00f","#f0f","#00f","#f0f","#00f","#f0f","#00f","#f0f","#00f",
    "#00f","#00f","#f0f","#00f","#00f","#00f","#f0f","#00f","#00f",
    "#00f","#f0f","#00f","#00f","#00f","#00f","#00f","#f0f","#00f",
    "#f0f","#00f","#00f","#00f","#f0f","#00f","#00f","#00f","#f0f",
    "#00f","#f0f","#00f","#00f","#00f","#00f","#00f","#f0f","#00f",
    "#00f","#00f","#f0f","#00f","#00f","#00f","#f0f","#00f","#00f",
    "#00f","#f0f","#00f","#f0f","#00f","#f0f","#00f","#f0f","#00f",
    "#00f","#00f","#00f","#00f","#f0f","#00f","#00f","#00f","#00f",
];
const [texw, texh] = [9, 9];

function pix(x, y) {
    ctx.fillRect(x, y, 1.5, 1.5);
}

function sq(a) {
    return a*a;
}

function dot(xy1, xy2) {
    return xy1[0]*xy2[0]+xy1[1]*xy2[1];
}

function xyd(xy1, xy2) {
    return [xy2[0]-xy1[0], xy2[1]-xy1[1]];
}

function bary(tri, x, y) { /* I don't get this :) */
    let [ds1, ds2, ds3] = [
        xyd(tri[0], tri[1]), xyd(tri[0], tri[2]), xyd(tri[0], [x, y]),
    ];
    let [d11, d12, d22, d31, d32] = [
        dot(ds1, ds1), dot(ds1, ds2), dot(ds2, ds2),
        dot(ds3, ds1), dot(ds3, ds2),
    ];
    let den = d11*d22-d12*d12;
    let b = (d22*d31-d12*d32)/den;
    let c = (d11*d32-d12*d31)/den;
    return [1-b-c, b, c];
}

function toutline(tri) {
    ctx.beginPath();
    ctx.moveTo(tri[0][0], tri[0][1]);
    ctx.lineTo(tri[1][0], tri[1][1]);
    ctx.lineTo(tri[2][0], tri[2][1]);
    ctx.lineTo(tri[0][0], tri[0][1]);
    ctx.stroke();
}

function binvalid(b) {
    return (b[0]<0||b[1]<0||b[2]<0);
}

function drawTriangle(tri, wi=0, hi=1) {
    let x1 = Math.min(tri[0][0], tri[1][0], tri[2][0]);
    let x2 = Math.max(tri[0][0], tri[1][0], tri[2][0]);
    let y1 = Math.min(tri[0][1], tri[1][1], tri[2][1]);
    let y2 = Math.max(tri[0][1], tri[1][1], tri[2][1]);
    x1 = Math.max(x1, 0);
    y1 = Math.max(y1, 0);
    x2 = Math.min(x2, cvs.width-1);
    y2 = Math.min(y2, cvs.height-1);
    for(let y = y1; y < y2; y++)
        for(let x = x1; x < x2; x++) {
            let b = bary(tri, x, y);
            if(binvalid(b)) continue;
            let tx = b[wi]*texw;
            let ty = b[hi]*texh;
            tx = Math.min(Math.floor(tx), texw-1);
            ty = Math.min(Math.floor(ty), texh-1);
            ctx.fillStyle = tex[ty*texw+tx];
            pix(x, y);
        }
}

let tris = [];

function pushTri(tri) {
    tris.push([tri[0].slice(), tri[1].slice(), tri[2].slice()]);
}

function pushQuad(q) {
    tris.push([q[0], q[2], q[3]]);
    tris.push([q[0], q[2], q[1]]);
}

function pushBox(box) {
    pushQuad([box[0], box[1], box[2], box[3]]);
    pushQuad([box[4], box[5], box[6], box[7]]);
    pushQuad([box[4], box[0], box[3], box[7]]);
    pushQuad([box[1], box[5], box[6], box[2]]);
    pushQuad([box[0], box[4], box[5], box[1]]);
    pushQuad([box[7], box[3], box[2], box[6]]);
}

function translate(xyz, txyz) {
    xyz[0] += txyz[0];
    xyz[1] += txyz[1];
    xyz[2] += txyz[2];
}

function rotx(xyz, a) {
    [xyz[0], xyz[1], xyz[2]] = [
        xyz[0],
        xyz[1]*Math.cos(a)-xyz[2]*Math.sin(a),
        xyz[1]*Math.sin(a)+xyz[2]*Math.cos(a),
    ];
}

function roty(xyz, a) {
    [xyz[0], xyz[1], xyz[2]] = [
        xyz[0]*Math.cos(a)+xyz[2]*Math.sin(a),
        xyz[1],
        -xyz[0]*Math.sin(a)+xyz[2]*Math.cos(a),
    ];
}

function rotz(xyz, a) {
    [xyz[0], xyz[1], xyz[2]] = [
        xyz[0]*Math.cos(a)-xyz[1]*Math.sin(a),
        xyz[0]*Math.sin(a)+xyz[1]*Math.cos(a),
        xyz[2],
    ];
}

function scale(xyz, s) {
    xyz[0] *= s;
    xyz[1] *= s;
    xyz[2] *= s;
}

function project(xyz) {
    xyz[0] *= 256/Math.max(xyz[2], 1);
    xyz[0] += cvs.width/2;
    xyz[1] *= 256/Math.max(xyz[2], 1);
    xyz[1] += cvs.height/2;
}

function triz(tri) {
    return (tri[0][2]+tri[1][2]+tri[2][2])/3;
}

function transform(s, op, opr) {
    for(let i = 0; i < s.length; i++)
        op(s[i], opr);
}

function drawTris() {
    let nb;
    do {
        nb = false;
        for(let i = 1; i < tris.length; i++)
            if(triz(tris[i-1]) < triz(tris[i])) {
                [tris[i-1], tris[i]] = [tris[i], tris[i-1]];
                nb = true;
            }
    } while(nb);
    tris.forEach(t => drawTriangle(t));
}

let tri = [
    [100, 10, 0],
    [200, 140, 0],
    [10, 180, 0],
];

let tri1 = [
    [100, 10, 0],
    [200, 140, 0],
    [300, 20, 0],
];

let box = [
    [-1, -1, -1],
    [1, -1, -1],
    [1, 1, -1],
    [-1, 1, -1],
    [-1, -1, 1],
    [1, -1, 1],
    [1, 1, 1],
    [-1, 1, 1],
];

let boxp = [0, 0, 0];
let rx=0, ry=0, rz=0;
let play = false;

function draw() {
    ctx.strokeStyle = "red";
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, cvs.width, cvs.height);
    //drawTriangle(tri);
    //drawTriangle(tri1);
    //toutline(tri);

    tris = [];
    let rbox = [];
    box.forEach(p => rbox.push(p.slice()));
    //console.log(rbox);
    transform(rbox, roty, -rx);
    transform(rbox, rotx, ry);
    //transform(rbox, rotz, -rx);
    transform(rbox, scale, cvs.width*0.005);
    transform(rbox, translate, [1, 0, 15]);
    transform(rbox, project);
    pushBox(rbox);
    drawTris();

    //drawTriangle([rbox[0], rbox[1], rbox[2]]);
}

draw();

cvs.onclick = function(e) {
    const r = cvs.getBoundingClientRect();
    const [x, y] = [e.clientX-r.left, e.clientY-r.top];
    const m = 0.2;
    if(x > cvs.width*0.75) rx += m;
    else if(x < cvs.width*0.25) rx -= m;
    if(y > cvs.height*0.75) ry += m;
    else if(y < cvs.height*0.25) ry -= m;
    else if(x >= cvs.width*0.25 && x <= cvs.width*0.75) play = !play;
    draw();
}

setInterval(function() {
    if(!play) return;
    rx += 0.1;
    ry += 0.07;
    draw();
}, 50);